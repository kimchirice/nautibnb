class AddUserToYachts < ActiveRecord::Migration[5.2]
  def change
    add_reference :yachts, :user, foreign_key: true
  end
end
