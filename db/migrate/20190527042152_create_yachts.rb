class CreateYachts < ActiveRecord::Migration[5.2]
  def change
    create_table :yachts do |t|
      t.string :name
      t.integer :max_guests
      t.string :photo
      t.integer :price_per_night
      t.text :description

      t.timestamps
    end
  end
end
