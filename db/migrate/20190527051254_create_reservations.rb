class CreateReservations < ActiveRecord::Migration[5.2]
  def change
    create_table :reservations do |t|
      t.references :user, foreign_key: true
      t.references :yacht, foreign_key: true
      t.integer :number_of_nights
      t.date :start_date
      t.integer :price_per_night
      t.string :status

      t.timestamps
    end
  end
end
