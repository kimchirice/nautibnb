# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

puts "Clean database..."

User.destroy_all
Yacht.destroy_all

# USERS
puts "Creating users"

user_list = [
  {
    email: "jack.sparrow@gmail.com",
    first_name: "Jack",
    last_name: "Sparrow",
    password: "password"
  }
]
User.create!(user_list)
puts "Created #{User.count} user(s)"

# YACHTS
puts "Creating yachts"
yachts_list = [
  {
    user_id: User.first.id,
    name: "Black Pearl",
    max_guests: 6,
    price_per_night: 200,
    description: "The Black Pearl is easily recognized by her distinctive black hull and sails. Captained by Jack Sparrow, she is said to be nigh uncatchable."
  },
  {
    user_id: User.first.id,
    name: "RMS Titanic",
    max_guests: 2500,
    price_per_night: 25000,
    description: "The name Titanic derives from the Titan of Greek mythology. Built in Belfast, Ireland, in the United Kingdom of Great Britain and Ireland (as it was then known), the RMS Titanic was the second of the three Olympic-class ocean liners—the first was the RMS Olympic and the third was the HMHS Britannic."
  },
  {
    user_id: User.first.id,
    name: "Santa Maria",
    max_guests: 4,
    price_per_night: 10000,
    description: "The Santa Maria, known as a clumsy, complicated vessel, famously transported Christopher Columbus from Spain to the “New World” in 1492. That same year on Christmas Day, the ship ran aground. But not all was lost — another ship, the Navidad, was built with its salvaged wood."
  },
  {
    user_id: User.first.id,
    name: "Boaty McBoatface",
    max_guests: 10,
    price_per_night: 7000,
    description: "Boaty McBoatface is the lead boat of the Autosub Long Range-class[3] of autonomous underwater vehicles (AUVs) used for scientific research that will be carried on the research vessel RRS Sir David Attenborough owned by the Natural Environment Research Council (NERC) and operated by the British Antarctic Survey (BAS)."
  },
  {
    user_id: User.first.id,
    name: "HMS Victory",
    max_guests: 500,
    price_per_night: 250,
    description: "Used by the British Royal Navy during the late 18th and early 19th century, HMS Victory is the world’s oldest commissioned warship. Currently, it serves as the Flagship of the First Sea Lord, while also acting as a “living” museum in England."
  },
]
Yacht.create!(yachts_list)
puts "Created #{Yacht.count} yacht(s)"

puts "Finish"
