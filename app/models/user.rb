class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  has_many :reservations
  has_many :reviews, through: :reservations

  has_many :wishlists

  has_many :owned_yachts, class_name: 'Yacht', foreign_key: :user_id # owned yachts

  has_many :booked_yachts, through: :reservations, source: :user
  has_many :favorite_yachts, through: :wishlists, source: :user
end
