class Yacht < ApplicationRecord
  has_many :reservations
  has_many :reviews, through: :reservations
  # has_many :wishlists

  belongs_to :user

  validates :name, :max_guests, :price_per_night, presence: true
  validates :price_per_night, numericality: { only_integer: true }
end
