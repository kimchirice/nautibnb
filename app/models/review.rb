class Review < ApplicationRecord
  belongs_to :reservation

  validates :rating, inclusion: { in: (0..5), allow_nil: false }
end
