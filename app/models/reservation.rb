class Reservation < ApplicationRecord
  belongs_to :user
  belongs_to :yacht
  has_many :reviews
end
