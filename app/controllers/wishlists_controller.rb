class WishlistsController < ApplicationController
  before_action :set_wishlist, only: :destroy

  def create
    @wishlist = Wishlist.new(wishlist_params)
    @wishlist.save
  end

  def destroy
    @wishlist.destroy
  end

  private

  def set_wishlist
    @wishlist = Wishlist.find(params[:wishlist_id])
  end

  def wishlist_params
    params.require(:wishlist).permit(:yacht_id, :user_id)
  end
end
