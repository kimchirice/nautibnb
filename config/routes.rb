Rails.application.routes.draw do
  devise_for :users
  root to: 'pages#home'

  resources :yachts, only: [ :index, :show ] do
    resources :reservations, only: :create
    resources :wishlists, only: :create
  end

  resources :reservations, only: :index do
    resources :reviews, only: :create

    member do
      patch :cancel
    end
  end

  resources :wishlists, only: :destroy
  resources :home, only: :index # This is the dashboard route
end

